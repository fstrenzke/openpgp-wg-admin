OpenPGP is an Internet standard that covers object encryption, object signing, and identity certification.
These were defined by the first incarnation of the OpenPGP working group.

The following is an excerpt from the charter of the original incarnation of the openpgp working group:

> The goal of the OpenPGP working group is to provide IETF standards for the algorithms and formats of PGP processed objects as well as providing the MIME framework for exchanging them via e-mail or other transport protocols.

The working group concluded this work and was closed in March of 2008.
In the intervening period, there has been a rough consensus reached that the RFC that defined the IETF openpgp standard, [RFC 4880](https://datatracker.ietf.org/doc/rfc4880/), is in need of revision.

This incarnation of the working group is chartered to primarily produce a revision of RFC4880 to address issues that have been identified by the community since the working group was originally closed.

These revisions will include, but are not necessarily limited to:

- Inclusion of elliptic curves recommended by the Crypto Forum Research Group (CFRG) (see note below)

- A symmetric encryption mechanism that offers modern message integrity protection (e.g. AEAD)

- Revision of mandatory-to-implement algorithm selection and deprecation of weak algorithms

- An updated public-key fingerprint mechanism

The Working Group will perform the following work:

- Revise RFC4880.
  The intent is to start from the current rfc4880bis draft.

- Other work related to OpenPGP may be entertained by the working group as long as it does not interfere with the completion of the RFC4880 revision.
  As the revision of RFC4880 is the primary goal of the working group, other work may be undertaken, so long as:

  1. The work will not unduly delay the closure of the working group after the revision is finished (unless the working group is rechartered).

  2. The work has widespread support in the working group.

These additional work items may only be added with approval from the responsible Area Director who may additionally require re-chartering for certain work items, as needed.

# Inclusion of CFRG Curves

The Working Group will consider CFRG curves as possible Mandatory to Implement (MTI) algorithms.

# Working Group Process

The working group will endeavor to complete most if not all of its work online on the working group's mailing list.
We expect that the requirement for face-to-face sessions at IETF meetings to be minimal.

For the revision of RFC 4880, all changes from RFC 4880, and for other work items, all content, require both consensus on the mailing list and the demonstration of interoperable support by at least two independent implementations, before being submitted to the IESG.

Furthermore, the working group will adopt no I-D's as working group items unless there is a review by at least two un-interested parties of the I-D as part of the adoption process.
